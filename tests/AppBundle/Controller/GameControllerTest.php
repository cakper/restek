<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GameControllerTest extends WebTestCase
{
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->client->followRedirects(true);
    }

    public function tearDown()
    {
        $this->client = null;
    }

    public function testTryWord()
    {
        $crawler = $this->client->request('GET', '/game/en/');
        $form = $crawler->selectButton('Let me guess...')->form();
        $crawler = $this->client->submit($form, ['word' => 'php']);

        $this->assertEquals('Congratulations!',
            $crawler->filter('#content h2')->text()
        );
    }
}
