<?php

namespace Tests\AppBundle\Game;

use AppBundle\Game\Game;
use AppBundle\Game\GameContext;
use Symfony\Component\HttpFoundation\Session\Session;

class GameContextTest extends \PHPUnit_Framework_TestCase
{
    public function testLoadTheGameFromSession()
    {
        $data = [
            'word' => 'php',
            'attempts' => 1,
            'tried_letters' => ['p', 'x'],
            'found_letters' => ['p']
        ];

        $session = $this->getMockBuilder(Session::class)
            ->setMethods(['get'])
            ->disableOriginalConstructor()
            ->getMock();

        $session->expects($this->once())
            ->method('get')
            ->with($this->equalTo('hangman'))
            ->will($this->returnValue($data));

        $context = new GameContext($session);

        $this->assertInstanceOf(Game::class, $context->loadGame());
    }
}
