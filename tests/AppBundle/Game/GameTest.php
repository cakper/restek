<?php

namespace Tests\AppBundle\Game;

use AppBundle\Game\Game;

class GameTest extends \PHPUnit_Framework_TestCase
{
    public function testTryCorrectWord()
    {
        $game = new Game('php');
        $this->assertTrue($game->tryWord('php'));
        $this->assertEquals(0, $game->getAttempts());
    }

    public function testTryCorrectLetter()
    {
        $game = new Game('php');
        $this->assertTrue($game->tryLetter('P'));
//        $game->isLetterFound('P') // true
//        $game->getFoundLetters() // assertContains('p'
//        $game->getTriedLetters( // assert contains
//        $game->getAttempts( // assert equals 0
    }

    public function testTryNumber()
    {
        $this->setExpectedException(\Exception::class);
        
        $game = new Game('php');
        $game->tryLetter(10);
    }
}
