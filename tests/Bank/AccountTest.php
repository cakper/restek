<?php

namespace Tests\Bank;

use Bank\BankAccount;
use Bank\BankAccountException;

class AccountTest extends \PHPUnit_Framework_TestCase
{
    public function testDefaultBalanceIsZero()
    {
        $account = new BankAccount();
        $this->assertSame(0, $account->getBalance());
    }

    public function testWithdrawNegativeAmount()
    {
        $this->setExpectedException(BankAccountException::class);

        $account = new BankAccount(500);
        $account->withdraw(-1000);
    }

    /**
     * @dataProvider provideMoneyToWithdraw
     */
    public function testWithdrawMoney($money, $newBalance)
    {
        $account = new BankAccount(1000);

        $this->assertSame($newBalance, $account->withdraw($money));
    }

    public function provideMoneyToWithdraw()
    {
        return [
            [100, 900],
            [500, 500]
        ];
    }
}
