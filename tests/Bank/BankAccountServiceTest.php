<?php

namespace Tests\Bank;

use Bank\BankAccount;
use Bank\CurrencyExchange;

class BankAccountServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testWithdrawForeignCurrencies()
    {
        $stub = $this->getMock(CurrencyExchange::class);
        $stub
            ->method('getExchangeRate')
            ->willReturn(1.20);

        $account = new BankAccount(1000); // EUR
        $service = new \Bank\BankAccountService($stub);

        $this->assertEquals(750, $service->withdraw($account, 300)); // 750 EUR, 300 USD
    }
}
