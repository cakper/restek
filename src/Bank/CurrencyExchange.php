<?php

namespace Bank;

interface CurrencyExchange
{
    public function getExchangeRate($from, $to);
}
