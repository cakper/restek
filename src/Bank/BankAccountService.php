<?php

namespace Bank;

class BankAccountService
{
    private $currencyExchange;

    public function __construct(CurrencyExchange $currencyExchange)
    {
        $this->currencyExchange = $currencyExchange;
    }

    public function withdraw(BankAccount $account, $money)
    {
        $rate = $this->currencyExchange->getExchangeRate('EUR', 'USD');

        return $account->withdraw($money / $rate);
    }
}
