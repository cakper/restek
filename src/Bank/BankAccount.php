<?php

namespace Bank;

class BankAccount
{
    private $balance;

    public function __construct($balance = 0)
    {
        $this->balance = $balance;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function withdraw($money)
    {
        if ($money < 0) {
            throw new BankAccountException();
        }

        $this->balance -= $money;

        return $this->balance;
    }
}
