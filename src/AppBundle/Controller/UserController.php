<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class UserController extends Controller
{
    /**
     * @Cache(smaxage=600)
     */
    public function usersAction()
    {
        $repo = $this->get('doctrine')->getRepository(User::class);
        $users = $repo->findAll();

        return $this->render('AppBundle:User:users.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/login", name="login")
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        $error = $session->get(Security::AUTHENTICATION_ERROR);
        $session->remove(Security::AUTHENTICATION_ERROR);

        $lastUsername = $session->get(Security::LAST_USERNAME);

        return $this->render('AppBundle:User:login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }


    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request)
    {
        $form = $this->createForm(UserType::class);
        $form->add('submit', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();

            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);
            $user->encodePassword($encoder);

            $em = $this->get('doctrine')->getManager();
            $em->persist($user);
            $em->flush();

            // persisting user to DB

            return $this->redirectToRoute('game_play');
        }

        return $this->render('AppBundle:User:register.html.twig', ['form' => $form->createView()]);
    }
}
