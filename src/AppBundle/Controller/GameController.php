<?php

namespace AppBundle\Controller;

use AppBundle\Game\GameContext;
use AppBundle\Game\GameRunner;
use AppBundle\Game\Loader\TextFileLoader;
use AppBundle\Game\Loader\XmlFileLoader;
use AppBundle\Game\WordList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/game/{_locale}/", requirements={"_locale"="%allowed_locale%"}, defaults={"_locale"="en"})
 */
class GameController extends Controller
{
    const VERSION = 1;

    /**
     * @Route("", name="game_play")
     * @Method("GET")
     */
    public function playAction()
    {
        $game = $this->get('app.game_runner')->loadGame($this->getParameter('app.word_length'));

        return $this->render('AppBundle:Game:play.html.twig', ['game' => $game]);
    }

    /**
     * @Route("/word", name="game_word", condition="request.request.has('word')")
     * @Method("POST")
     */
    public function playWord(Request $request)
    {
        $game = $this->get('app.game_runner')->playWord(strtolower($request->request->get('word')));

        return $this->redirectToRoute($game->isWon() ? "game_won" : "game_failed");
    }

    /**
     * @Route("/letter/{letter}", name="game_play_letter", requirements={"letter"="[a-z]"})
     * @Method("GET")
     */
    public function playLetterAction($letter)
    {
        $game = $this->get('app.game_runner')->playLetter($letter);

        if (!$game->isOver()) {
            return $this->redirectToRoute("game_play");
        }

        return $this->redirectToRoute($game->isWon() ? "game_won" : "game_failed");
    }

    /**
     * @Route("/win", name="game_won")
     * @Method("GET")
     */
    public function winAction()
    {
        $game = $this->get('app.game_runner')->resetGameOnSuccess();

        return $this->render('AppBundle:Game:win.html.twig', ['game' => $game]);
    }

    /**
     * @Route("/failed", name="game_failed")
     * @Method("GET")
     */
    public function failedAction()
    {
        $game = $this->get('app.game_runner')->resetGameOnFailure();

        return $this->render('AppBundle:Game:failed.html.twig', ['game' => $game]);
    }

    /**
     * @Route("/reset", name="game_reset")
     * @Method("GET")
     */
    public function resetAction()
    {
        $this->get('app.game_runner')->resetGame();

        return $this->redirectToRoute("game_play");
    }
}
