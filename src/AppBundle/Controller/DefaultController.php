<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('AppBundle:Default:index.html.twig', ['name' => 'Anonymous']);
    }

    /**
     * @Route("/users", name="list_users")
     */
    public function listAction(Request $request)
    {
        $users = ['Kacper', 'Travis', 'Chris', 'Kent', 'Bob'];

        $response = new Response();
        $response->setEtag(md5(serialize($users)));

        if ($response->isNotModified($request)) {
            return $response;
        }

        return $this->render('AppBundle:Default:list.html.twig', ['users' => $users], $response);
    }


    /**
     * @Route("/{_locale}/test", name="test_locale")
     */
    public function testAction()
    {
        $users = ['Kacper', 'Travis', 'Chris', 'Kent'];
        return $this->render('AppBundle:Default:list.html.twig', ['users' => $users]);
    }

    public function randomAction()
    {
        return new Response('<div>'.rand(0, 10).'</div>');
    }

    /**
     * @Route("/greet/{name}", name="greet")
     */
    public function greetAction($name)
    {
        return $this->render('AppBundle:Default:index.html.twig', ['name' => '<strong>' . $name . '</strong>']);
    }
}
