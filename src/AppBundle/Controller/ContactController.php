<?php

namespace AppBundle\Controller;

use AppBundle\Contact\ContactRequest;
use AppBundle\Contact\ContactRequestType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller
{
    /**
     * @Route("/contact", name="contact")
     */
    public function contactAction(Request $request)
    {
        $form = $this->createForm(ContactRequestType::class);
        $form->add('submit', SubmitType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var $contactRequest ContactRequest */
            $contactRequest = $form->getData();
            $message = $contactRequest->toSwiftMessage($this->get('templating'));
            $this->get('mailer')->send($message);

            $this->addFlash('notice', 'Contact successful');

            return $this->redirectToRoute('game_play');
        }

        return $this->render('AppBundle:Contact:form.html.twig', ['form' => $form->createView()]);
    }
}
