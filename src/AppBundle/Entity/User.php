<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="user", uniqueConstraints={
 *     @ORM\UniqueConstraint("user_username_unique", columns="username"),
 *     @ORM\UniqueConstraint("user_email_address_unique", columns="email_address")
 * })
 * @ORM\Entity()
 *
 * @UniqueEntity("username")
 * @UniqueEntity("emailAddress")
 */
class User implements UserInterface
{
    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\GeneratedValue()
     * @ORM\Id()
     */
    public $id;

    /**
     * @ORM\Column(length=25)
     * @Assert\NotBlank()
     * @Assert\Length(min=5, max=25)
     */
    public $username;

    /**
     * @ORM\Column(length=100)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public $emailAddress;

    /**
     * @ORM\Column(length=100)
     * @Assert\NotBlank()
     * @Assert\Length(min=5, max=50)
     */
    public $fullName;

    /**
     * @ORM\Column()
     */
    public $password;

    public $rawPassword;

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return '';
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function eraseCredentials()
    {
        $this->rawPassword = null;
    }

    public function encodePassword(PasswordEncoderInterface $passwordEncoder)
    {
        if (!is_null($this->rawPassword)) {
            $this->password = $passwordEncoder->encodePassword($this->rawPassword, '');
            $this->eraseCredentials();
        }
    }
}
