<?php

namespace AppBundle\Model;

class User
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function printName()
    {
        return $this->name . "!";
    }
}
