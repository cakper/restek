<?php

namespace AppBundle\Contact;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactRequestType extends AbstractType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContactRequest::class,
            'translation_domain' => 'contact'
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', TextType::class, ['label' => 'contact.full_name'])
            ->add('emailAddress', EmailType::class)
            ->add('subject', TextType::class, ['attr' => ['maxlength' => 100]])
            ->add('message', TextareaType::class, ['attr' => ['rows' => 20, 'cols' => 50]]);
    }
}
