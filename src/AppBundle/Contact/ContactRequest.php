<?php

namespace AppBundle\Contact;

use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ContactRequest
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=5, max=40)
     */
    public $fullName;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public $emailAddress;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     */
    public $subject;

    /**
     * @Assert\NotBlank()
     */
    public $message;

    public function toSwiftMessage(EngineInterface $templating)
    {
        $message = \Swift_Message::newInstance()
            ->setTo('kacper@gunia.me')
            ->setFrom('contact@example.com')
            ->setSender('contact@example.com')
            ->setReplyTo($this->emailAddress, $this->fullName)
            ->setSubject($this->subject)
            ->setBody($templating->render("AppBundle:Contact:message.txt.twig", ['contact' => $this]));

        return $message;
    }
}
