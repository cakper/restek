<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GreetCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:greet')
            ->setDescription('Say hello to someone')
            ->addArgument('who', InputArgument::REQUIRED, 'Person to greet')
            ->addOption('scream', 's', InputOption::VALUE_NONE, 'Scream at the person');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('who');
        $scream = (bool)$input->getOption('scream');
        
        if ($scream) {
            $name = strtoupper($name);
        }

        $output->writeln('Hello ' . $name . '!');
    }
}
